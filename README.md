# crops-init-build-env

Wrap CROPS image to be used with Docker like Yocto's native
`oe-init-build-env`.

## Usage

Use it like you would use `oe-init-build-env`, run:

```bash
source crops-init-build-env
```

in your `poky` directory. Then you can run `bitbake`, `devtool` and `runqemu`
like you could after sourcing the `oe-init-build-env` with the difference
that the command is run inside the CROPS image.

### Variables

When sourcing the script `TEMPLATECONF` can be when sourcing
`crops-oe-init-env`.

If other variables needs to be passed prefix the variable with `YC_`. For
example to pass a `MACHINE` environment flag through for `bitbake`:

```bash
YC_MACHINE=rpi bitbake core-image-minimal
```

### Docker image

By default `crops/poky:latest` image is used. Different image can be used by
specifying `IMAGE` when sourcing the script:

```bash
IMAGE=crops/poky:fedora-35 source crops-init-build-env
```
